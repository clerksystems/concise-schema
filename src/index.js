'use strict';

const 	path					= require( 'path' )
,		ValueValidator			= require( path.join( __dirname, 'ValueValidator' ))
,		SchemaValidator			= require( path.join( __dirname, 'SchemaValidator' ))
,		types					= require( path.join( __dirname, 'common', 'types' ))
;



module.exports = {

	get value()
	{
		return new ValueValidator();
	}

,	'validate' 		: SchemaValidator.validate
,	'generate' 		: SchemaValidator.generate
,	'onLoadReset' 	: SchemaValidator.onLoadReset
,	'toObject' 		: SchemaValidator.toObject

,	get types()
	{
		return types;
	}
};