'use strict';


const 	path		= require( 'path' )
,		types		= require( path.join( __dirname, 'common', 'types' ))
;


/**
 * @todo
 * getting an unknown property does not result in an error,
 * this could be fixed by using a proxy (dynamic getters)
 */


const ValueValidator = function()
{
	this.allowedTypes 		= new Set();
	this.options			= new Set();
	this.isRequired			= false;
	this.defaultValue		= undefined;
	this.minValue			= undefined;
	this.maxValue			= undefined;
	this.subValue			= undefined;
	this.init				= true;


	this.subObjValues		= false			// validate the values of the sub object (disregard the keys)
	this.isLocked			= false
	this.resetOnLoad		= false

	this.isSetOnce			= false
	this.wasChanged			= false
};





const setSubType = function( subVal )
{
	let ctx = checkContext( this );
	// if ( ctx.subObjValues === true )
	// {
	// 	subVal.objValuesAsArray = true
	// 	// console.log( subVal )
	// }
	ctx.subValue = subVal;
	return ctx;
}


ValueValidator.prototype = {

	'isVV' : true

// ,	get of()
// 	{
// 		console.log( 'OF' )
// 		let ctx = checkContext( this );
// 		ctx.subValue = new ValueValidator();
// 		return ctx.subValue;
// 	}

,	like : setSubType
,	with : setSubType
}



const prototypeMethods = {

	'addType' 		: ( o, type ) => o.allowedTypes.add( type )

,	'min' 			: ( o, val ) => o.minValue = val

,	'max' 			: ( o, val ) => o.maxValue = val

,	'enum' 			: function( o ) { [...arguments].slice( 1 ).forEach( ( item ) => o.options.add( item ) ) }

,	'default'		: ( o, val ) => o.defaultValue = val


// add methods here!

};


for ( let methodName in prototypeMethods )
{
	ValueValidator.prototype[ methodName ] = function()
	{
		let ctx = checkContext( this );
		prototypeMethods[ methodName ]( ctx, ...arguments );
		return ctx;
	}
}


Object.keys( types ).map( ( type ) =>

	ValueValidator.prototype.__defineGetter__(

		type

	,	function()
		{
			let ctx = checkContext( this );
			return ctx.addType( type );
		}
	)
);





const prototypeGetters = {
	required 		: ( o ) => o.isRequired = true
,	optional 		: ( o ) => o.isRequired = false
,	or	 			: ( o ) => o

// add getters here:
,	lock 			: ( o ) => o.isLocked = true
,	values 			: ( o ) =>
					{
						o.subObjValues = true
					}
,	setonce 		: ( o ) => o.isSetOnce = true
,	resetonload		: ( o ) => o.resetOnLoad = true
};

prototypeGetters.req = prototypeGetters.required;
prototypeGetters.opt = prototypeGetters.optional;




for ( let getterName in prototypeGetters )
{
	ValueValidator.prototype.__defineGetter__(

			getterName

		,	function()
			{
				let ctx = checkContext( this );

				prototypeGetters[ getterName ]( ctx );

				return ctx;
			}
		);
}


const checkContext = ( ctx ) =>
{
	if ( ctx.init === true )
	{
		var newCtx = new ValueValidator();
		newCtx.init = false;
		return newCtx;
	}
	return ctx;
}


module.exports = ValueValidator;
