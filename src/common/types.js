'use strict';

const 	path 		= require( 'path' )
,		regexes		= require( path.join( __dirname, 'regexes' ) )
,		iWidthValidator = (width) => (val) =>
							types.number.validator(val)
							&&
							val % 1 === 0
							&&
							val <= Math.min(Number.MAX_SAFE_INTEGER, Math.pow(2, isNaN( witdh ) ? 53 : width))
							&&
							val >= Math.max(Number.MIN_SAFE_INTEGER, -1*Math.pow(2, isNaN( witdh ) ? 53 : width))

,		uiWidthValidator = (width) => (val) =>
							types['unsigned number'].validator(val)
							&&
							val % 1 === 0
							&&
							val <= Math.min(Number.MAX_SAFE_INTEGER, Math.pow(2, isNaN( witdh ) ? 53 : width))
;

let 	types 		= {
						'any'		: {
							'default'		: ''
						,	'validator'		: ( val ) => true
						}
					,	'string'		: {
							'default'		: ''
						,	'validator'		: ( val ) => typeof( val ) === 'string'
						}
					,	'unsigned number': {
							'default'		: 1
						,	'validator'		: ( val ) =>
												types.number.validator( val )
												&&
												val >= 0
						}
					,	'number'		: {
							'default'		: 1
						,	'validator'		: ( val ) =>
												typeof( val ) === 'number'
												&&
												Number.isFinite( val )
												&&
												isNaN( val ) === false

						}
					,	'array'		: {
							'default'		: []
						,	'validator'		: ( val ) => Object.prototype.toString.call( val ) === '[object Array]'
						}
					,	'object'		: {
							'default'		: {}
						,	'validator'		: ( val ) => Object.prototype.toString.call( val ) === '[object Object]'
						}
					,	'date'		: {
							'default'		: new Date()
						,	'validator'		: ( val ) => {
												if ( Object.prototype.toString.call( val ) === '[object Date]' )
												{
													return true;
												}
												else
												{
													return ! isNaN( Date.parse( val ) )
												}
											}
						}
					,	'email'		: {
							'default'		: 'john@doe.com'
						,	'validator'		: ( val ) => regexes.email.test( val )
						}
					,	'hostname'		: {
							'default'		: 'google.com'
						,	'validator'		: ( val ) => 
											{	// https://github.com/zaggino/z-schema/blob/master/src/FormatValidators.js
												var valid = regexes.hostname.test(hostname);
												if (valid) {
													// the sum of all label octets and label lengths is limited to 255.
													if (hostname.length > 255) { return false; }
													// Each node has a label, which is zero to 63 octets in length
													var labels = hostname.split(".");
													for (var i = 0; i < labels.length; i++) { if (labels[i].length > 63) { return false; } }
												}
												return valid;
											}
					}
					,	'boolean'		: {
							'default'		: true
						,	'validator'		: ( val ) => typeof( val ) === 'boolean'
					}
					,	'emptyString' 	: {
							'default'		: ''
						,	'validator'		: ( val ) => val === ''
					} 
					,	'null'		 	: {
							'default'		: null
						,	'validator'		: ( val ) => val === null
					}
					,	'integer'		: {
							'default'		: 1
						,	'validator'	: iWidthValidator(31)
						}
					,	'unsigned integer': {
							'default'		: 1
						,	'validator'	: uiWidthValidator(32)
						}
					,	'short'		: {
							'default'		: 1
						,	'validator'	: iWidthValidator(15)
						}
					,	'unsigned short': {
							'default'		: 1
						,	'validator'	: uiWidthValidator(16)
						}
					,	'char'		: {
							'default'		: 1
						,	'validator'	: iWidthValidator(7)
						}
					,	'unsigned char': {
							'default'		: 1
						,	'validator'	: uiWidthValidator(8)
						}
				}
;

types.int32_t				= types.integer;
types.uint32_t				= types['unsigned integer'];
types.int16_t				= types.short;
types.uint16_t				= types['unsigned short'];
types.int8_t				= types.char;
types.uint8_t				= types['unsigned char'];
types.byte					= types.char;
types['unsigned byte']		= types['unsigned char'];
types['unsigned float']		= types['unsigned number'];
types['unsigned double']	= types['unsigned number'];
types.double				= types.number;
types.float					= types.number;
types.bool					= types.boolean;
types.str 					= types.string;
types.num 					= types.number;
types.arr 					= types.array;
types.obj 					= types.object;

module.exports = types;