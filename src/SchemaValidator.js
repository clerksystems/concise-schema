'use strict'

const	types			= require( './common/types.js' )
const	isObject		= types.object.validator

const	ValueValidator	= require( './ValueValidator.js' )
const	v				= new ValueValidator()


const onLoadReset = function( schema, obj, path ){

	// no schema... just return
	if( schema === undefined ){
		return undefined
	}

	// parsing args
	let oldState
	let originalState
	let migrations

	if( obj ){
		oldState = obj
		originalState = JSON.parse( JSON.stringify( obj ) )
	} else {
		oldState = schema.oldState
		originalState = schema.originalState ? schema.originalState : JSON.parse( JSON.stringify( schema.oldState ) )
		migrations = schema.migrations
		path = schema.path
		schema = schema.schema
	}

	// console.log(path, migrations)

	let version = originalState.version

	if( schema.isVV !== true ){
		// check object
		if ( isObject( schema ) === true ){
			schema = new ValueValidator().required.object.like( schema );
		}
		// check array
		else if ( types.array.validator( schema ) === true ){
			schema =
				schema.length === 1
				?	new ValueValidator().required.array.like( schema[ 0 ] )
				:	new ValueValidator().required.array
				;
		}
	}

	// reset
	if ( schema.resetOnLoad === true ){
		return generate( schema, path )
	}

	// check sub value
	else if( isObject( schema.subValue ) && !schema.allowedTypes.has( 'array' ) && !schema.subObjValues ){
		return Object.keys( schema.subValue )
				.reduce(
					( newState, schemaKey ) => {
						const _path = path ? `${path}.${schemaKey}` : `${schemaKey}`

						// if migrations exists, use it
						if( migrations && migrations[ _path ] ){
							// console.log( 'Migrating:', 'path', _path, `version "${version}"` )
							// console.log( ' >>. ', migrations[ _path ][ version ? version : '' ] )

							// if path is a string put it in an array
							if( !Array.isArray( migrations[ _path ] ) ){
								migrations[ _path ] = [ migrations[ _path ] ]
							}

							// for each path try to migrate the content
							// console.log( '..', _path, migrations[ _path ] )
							for( let p of migrations[ _path ] ){
								// console.log(' >> Migrating:', p )
								try {
									if( typeof p === 'function' ){
										oldState[ schemaKey ] = p( originalState )
									} else if( Array.isArray( p ) ){
										oldState[ schemaKey ] = p.reduce( (thing, path) => {
																try {
																		return path.split('.')
																			.reduce( ( thing, key ) => {
																				if( thing[ key ] === undefined ){
																					// console.log(`Can't find ${p}`)
																					throw new Error( `Can't find ${key}` )
																				}
																				return thing[ key ]
																			}, thing)
																} catch( err ) {
																	// skip
																}
															}, originalState )
									} else {
										oldState[ schemaKey ] = p.split( '.' )
																	.reduce( (thing, key) => {
																		if( thing[ key ] === undefined ){
																			// console.log(`Can't find ${p}`)
																			throw new Error( `Can't find ${p}` )
																		}
																		return thing[ key ]
																	}, originalState )
									}
									break
								} catch( err ) {
									console.log( `Migration step failed: ${p}->${_path}    ${err.message}` )
								}

							}
						}

						// console.log( '>>..', oldState[ schemaKey ] )
						if( oldState[ schemaKey ] === undefined ){
							oldState[ schemaKey ] = generate( schema.subValue[ schemaKey ], _path )
						}

						// recurse loading
						newState[ schemaKey ] = onLoadReset( {
							oldState: oldState[ schemaKey ],
							schema: schema.subValue[ schemaKey ],
							migrations,
							originalState,
							path: _path
						})

						// console.log( 'NS:', newState )
						return newState
					}
				,	{}
				)
	}

	return oldState
}

const	validate			= function( schema, obj, throwOnFirstError, path = '' )
							{
								throwOnFirstError = throwOnFirstError === undefined ? true : !! throwOnFirstError ;


								// internal error handling - can either accumulate
								// or throw on first error

								let errors = [];

								const error = ( err ) =>
								{
									if ( types.array.validator( err ) )
									{
										return err.map( error );
									}

									errors.push( err );

									if ( throwOnFirstError === true )
									{
										throw new Error( err );
									}
								};



								// if an expression evaluated to undefined, there is
								// an error in the schema

								if ( schema === undefined )
								{
									error( 'Undefined value validator at [' + path + '] - did you use a non existant type or specificator?' );
								}



								// replace object and array literals with expressions

								if ( ! ( schema.isVV === true ) )
								{
									if ( types.object.validator( schema ) === true )
									{
										schema = v.required.object.like( schema );
									}
									else if ( types.array.validator( schema ) === true )
									{
										schema = v.required.array.like( schema.length === 1 ? schema[ 0 ] : v.any );
									}
									else
									{
										error( 'Unknown expression: [' + schema + ']' );
									}
								}


								// validate the expression

								if ( schema.isVV === true )
								{
									// console.log( obj, schema )
									// if no value is given and none is required, we cool

									if ( obj === undefined && schema.isRequired === false )
									{}


									// if this is an enum, check whether
									// the value is one of the options

									else if ( schema.options.size > 0 )
									{
										if ( ! schema.options.has( obj ) )
										{
											error( 'Option ' + obj + ' not specified in enum ' + [ ...schema.options ].join( ',' ) );
										}
									}

									// if the value is locked
									// check that it matches the default

									else if ( schema.isLocked === true )
									{
										if ( obj !== schema.defaultValue )
										{
											error( `Trying to mutate the locked value at ${ path } from ${ schema.defaultValue } to ${ obj }` )
											// obj = schema.defaultValue
										}
									}



									// setOnce has to be checked elsewhere (in the store?)
									// as the validator has no historical data



									// check whether one of the allowed types
									// matches the type of the target

									else
									{
										let validType = undefined;

										const vScore = [ ...schema.allowedTypes ]
											.reduce(
												function( validations, type )
												{
													if ( typeof( types[ type ] ) === undefined )
													{
														error( 'unknown type: ' + type );
													}
													else
													{
														const result = types[ type ].validator( obj ) ? 1 : 0;
														validType = result === 1 ? validType || type : validType;
														return validations + result;
													}
												}, 0 )
										;

										if ( vScore > 0 )
										{
											// one or more types were matched
											// the value's type is valid

											const objType = typeof( obj )

											if ( schema.minValue )
											{
												if ( Array.isArray( obj ) )
												{
													if ( obj.length < schema.minValue )
													{
														error( 'Array length below minimum at path "' + path + '"' )
													}
												}
												else if ( objType === 'number' )
												{
													if ( obj < schema.minValue )
													{
														error( 'Number below minimum at path "' + path + '"' )
													}
												}
												else if ( objType === 'string' )
												{
													if ( obj.length < schema.minValue )
													{
														error( 'String length below minimum at path "' + path + '"' )
													}
												}
											}

											if ( schema.maxValue )
											{
												if ( Array.isArray( obj ) )
												{
													if ( obj.length > schema.maxValue )
													{
														error( 'Array length above maximum at path "' + path + '"' )
													}
												}
												else if ( objType === 'number' )
												{
													if ( obj > schema.maxValue )
													{
														error( 'Number above maximum at path "' + path + '"' )
													}
												}
												else if ( objType === 'string' )
												{
													if ( obj.length > schema.maxValue )
													{
														error( 'String length above maximumat path "' + path + '"' )
													}
												}
											}

											// check if this type has nested
											// entries that have to be checked

											// console.log( validType, schema.subObjValues )

											if ( schema.subValue !== undefined )
											{
												if ( validType === 'object' || validType === 'obj' )
												{

													if ( types.object.validator( schema.subValue ) !== true )
													{
														error( 'Unexpected like specification for object' );
													}
													else if ( schema.subObjValues === true )
													{
														// console.log( `Validating [${Object.keys(obj)}]`  )
														error(
															Object.keys( obj )
																.reduce(
																	( totalErrors, entry ) =>
																		totalErrors.concat(
																			validate( schema.subValue, obj[ entry ], throwOnFirstError, path + '.' + entry )
																		)
																,	[]
																)
														)
													}
													else
													{
														error(
															Object.keys( schema.subValue ).reduce(

																( totalErrors, entry ) => totalErrors.concat( validate( schema.subValue[ entry ], obj[ entry ], throwOnFirstError, path + '.' + entry ) )

															,	[]
															)
														);
													}
												}
												else if ( validType === 'array' || validType === 'arr' )
												{
													error(
														obj.reduce(

															( totalErrors, entry, i ) => totalErrors.concat( validate( schema.subValue, entry, throwOnFirstError, path + '[' + i + ']' ) )

														,	[]
														)
													);
												}
											}
										}
										else
										{
											error( 'expected a "' + [ ...schema.allowedTypes ].join( '" or "' ) + '", got "' + obj + '", at path "' + path + '"' );
										}
									}
								}

								return errors;
							};


const	normalizeSchema		= schema =>
							{
								if ( ! ( schema.isVV === true ) )
								{
									if ( types.object.validator( schema ) === true )
									{
										schema = new ValueValidator().required.object.like( schema );
									}
									else if ( types.array.validator( schema ) === true )
									{
										schema =
											schema.length === 1
											?	new ValueValidator().required.array.like( schema[ 0 ] )
											:	new ValueValidator().required.array
											;
									}
								}

								return schema
							}

const	generate			= function( schema, path = '' )
							{
								if ( schema === undefined )
								{
									throw new Error( 'Undefined value validator at [' + path + '] - did you use a non existant type or specificator?' );
								}

								schema = normalizeSchema( schema )

								// generate defaults

								if ( schema.isVV === true )
								{
									// if this is an enum, return the first option

									if ( schema.options.size > 0 )
									{
										return schema.defaultValue !== undefined ? schema.defaultValue : [ ...schema.options] [ 0 ];
									}

									const 	allowedTypes 	= [ ...schema.allowedTypes ]
									,		defType			= allowedTypes.length === 0 ? 'any' : allowedTypes[ 0 ]
									;

									if ( ( defType === 'object' || defType === 'obj' ) && schema.subValue !== undefined && schema.subObjValues === false )
									{
										if ( types.object.validator( schema.subValue ) !== true )
										{
											throw new Error( 'Unexpected like specification for object' );
										}
										else
										{
											// console.log( 'obj', schema.subValue[ 'a' ], generate( schema.subValue[ 'a' ] ) )
											return Object.keys( schema.subValue ).reduce(

												( returnObj, key ) =>
												{
													returnObj[ key ] = generate( schema.subValue[ key ], `${path}.${key}` );
													return returnObj;
												}

											,	{}
											);
										}
									}
									if ( ( defType === 'object' || defType === 'obj' ) && schema.subValue !== undefined && schema.subObjValues === true )
									{
										// if its an object with a values and a like specification, check subvalue and return empty object
										if ( types.object.validator( schema.subValue ) !== true )
										{
											throw new Error( 'Unexpected like specification for object' );
										}
										else
										{
											return schema.defaultValue
										}
									}
									// else if ( ( defType === 'array' || defType === 'arr' ) && schema.subValue !== undefined )
									// {
									// return [ generate( schema.subValue ) ];
									// }
									else
									{
										if ( typeof( schema.defaultValue ) !== 'undefined' )
										{
											const valErrors = validate( schema, schema.defaultValue, false )

											if ( valErrors.length > 0 )
											{
												throw new Error( `Default value '${ schema.defaultValue }' at '${ path }' is incompatible with its validation constraints: ${ valErrors.join( ';' ) }` )
											}

											return schema.defaultValue;
										}

										return JSON.parse( JSON.stringify( types[ defType ].default ) );
									}
								}
							};


const	toObject			= ( schema, path = '' ) =>
							{
								schema			= normalizeSchema( schema )

								// console.log( path )
								// if ( ! path ) console.log( JSON.stringify( schema, null, 2 ) )

								const	obj		= Object.keys( schema )
													.reduce(
														( obj, key ) =>
														{
															// console.log( key )
															switch ( key )
															{
																case 'allowedTypes':
																case 'options':
																	obj[ key ] = Array.from( schema[ key ] )
																	break;
																case 'minValue':
																case 'maxValue':
																case 'isLocked':
																case 'isRequired':
																	obj[ key ] = schema[ key ]
																	break;
																case 'subValue':
																	if ( schema[ key ] )
																	{
																		if ( schema[ key ].isVV )
																		{
																			obj[ key ] 	= toObject(
																							schema[ key ]
																						,	`${path}${path==='' ? '' : '.' }${key}`
																						)
																		}
																		else
																		{
																			obj[ key ]	= Object.keys( schema[ key ] )
																							.reduce(
																								( subObj, subKey ) =>
																								{
																									// console.log( path, '.', subKey )
																									return Object.assign(
																										subObj
																									,	{
																											[subKey]	: toObject(
																															schema[ key ][ subKey ]
																														,	`${path}${path==='' ? '' : '.' }${subKey}`
																														)
																										}
																									)
																								}
																							,	{}
																							)
																		}
																	}
																	break;
															}
															return obj
														}
													,	{}
													)

								return obj
							}


module.exports = {
	validate,
	generate,
	toObject,
	onLoadReset
}
