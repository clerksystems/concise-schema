# Concise Schema

## Objective
To validate data structures against a concisely defined type schema.

## Example
```javascript
{
    meta: {
        host: v.string,
        publicIp: v.string,
        owner: v.email,
        startTime: v.date
    },
    connections: v.array.resetonload.like(
        {
            id: v.any,
            remote: v.string,
            port: v.number,
            connected: v.boolean.default( false ),
            close: v.function
        }
    ),
    world: v.object.like({
        nodes: v.array
    ,   name: v.string.required
    })
}
```

## Migrating state to a new schema
Migrating from the old state to a new schema is done with the migrations options in onLoadReset(); If migration fails for a path, that path is generated from the schema. Migrating from very old state version to a new schema can be done by listing where the properties used to be in older versions.

### Migrations syntax
```javascript
{
	newPath: oldPath,
	newPath: [ ...oldPaths ],
	newPath: state => _.get( state, oldPath )
}
```

### Example migrations
``` javascript
{ // schema version 1.0.0
	hello: v.str.default( 'Hello World' )
}
{ // schema version 1.0.1
	string: v.str.default( 'Hello World' )
}
{ // schema version 1.0.2
	message: v.str.default( 'Hello World' )
}
// migration schema for 1.0.0 -> 1.0.1 -> 1.0.2
{
	string: 'hello',
	message: 'string'
}
{
	message: ['hello', 'string']
}
```


## To-Do
* Returned schema path in errors is incorrect for objects with dynamic keys (currently returns an array index). Might be solved by treating all arrays as objects.
* Whene generating the default data structure from a schema, no attention is paid to min/max attributes in the schema. The manually entered default values should match the min/max attribute. This should be fixed.