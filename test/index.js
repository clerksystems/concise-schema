'use strict';

const 	path 			= require( 'path' )
,		expect			= require( 'chai' ).expect
,		validator		= require( path.join( __dirname, '..', 'src', 'index' ) )
,		v 				= validator.value
,		generate		= validator.generate
;


const schemas = {
		'rawStr'			: v.string
	,	'rawArr'			: [ v.string ]
	,	'str' 				: { a : v.string }
	,	'num' 				: { a : v.number }
	,	'bool'				: { a : v.boolean }
	,	'date'				: { a : v.date }
	,	'email'				: { a : v.email }
	,	'object'			: { a : v.object }
	,	'emptyString'		: { a : v.emptyString }
	,	'null'				: { a : v.null }
	,	'array'				: { a : v.array }
	,	'arrayOfString'		: { a : v.array.with( v.string ) }
	,	'arrayOrObject'		: { a : v.array.or.object }
	,	'optStr'			: { a : v.opt.string }
	,	'reqStr'			: { a : v.req.string }
	,	'min4Str'			: { a : v.string.min(4).default( 'abcd' ) }
	,	'max10Str'			: { a : v.string.max(10).default( 'abcdefghij' ) }
	,	'min4Array'			: { a : v.array.min(4).default( [ 'a', 'b', 'c', 'd' ] ) }
	,	'max10Array'		: { a : v.array.max(10).default( 'abcdefghij'.split() ) }
	,	'min4Num'			: { a : v.number.min(4).default( 4 ) }
	,	'max10Num'			: { a : v.number.max(10).default( 10 ) }
	,	'nestedNumber'		: { a : { 'b' : v.number } }
	,	'optNestedNumber'	: { a : v.opt.object.like({ 'b' : v.number }) }
	,	'arrayedNumber'		: { a : v.opt.array.like( v.number ) }
	,	'arrayedAny'		: { a : v.opt.array.like( v.any ) }
	,	'reqDefaultString'	: { a : v.req.str.default( 'default string 1' ) }
	,	'optDefaultString'	: { a : v.opt.str.default( 'default string 2' ) }
	,	'defaultFalse'		: { a : v.bool.default( false ) }
	,	'enum'				: { a : v.enum( 'foo', 'faa' ) }
	,	'enumWithDefault'	: { a : v.enum( 'foo', 'faa' ).default( 'faa' ) }
	,	'enumWithDefault2'	: { a : v.enum( true, 'pending', false ).default( false ) }
	,	'user'				: {
								'firstName' : v.req.string.min(2).default( 'henk' )
							,	'email' 	: v.req.email.or.emptyString
							,	'age' 		: v.req.date
							,	'grade' 	: v.opt.number.min(4).max(45).default( 8 )
							,	'hobbies'	: v.opt.array.like( v.string )
							,	'clothes'	: v.opt.object.like(
								{
									'trousers' 	: v.req.enum( 'long', 'short' )
								,	'shirt' 	: v.opt.string.default( 'black' )
								})
							}
	,	'company'			: {
								'company'		: v.string.default( 'Clerk Systems' )
							,	'email'			: v.email
							,	'employees' 	: [{
													'name' : v.str
												,	'birthDay' : v.date
												,	'tasks' : []
												}]
							}
	,	'log'				: [{
								't'	: {
									'up'	: v.any.default( null )		// start up time
								,	'down'	: v.any.default( null )
								}
							,	'e' : []										// boot error(s)
							,	'i'	: {
									'v'		: v.num.or.str.default( '?' )		// version
								}
							}]
	,	'locked'			: { l : v.str.lock.default( 'I am locked' ) }
	,	'objValues'			: { a : v.obj.values.like( v.str.req ) }
	,	'objValuesOpt'		: { a : v.obj.values.like( v.str ) }
	,	'objValuesObj'		: { a : v.obj.values.like({ x: v.str }) }
	,	'filter'			: v.obj.values.like({
										name		: v.str,
										frequency	: v.num.str.default( 1000 ),
										qFactor		: v.num.str.default( Math.sqrt( 2 ) ),
										gain		: v.num.str.default( 0 ),
										type		: v.str,
										enabled		: v.bool.default( false ),
										state		: v.str.default( 'disabled' ).enum( 'disabled', 'enabled', 'error' ),
										error		: v.array.like( v.str )
									})
	,	'filterReq'			: v.obj.values.like({
										name		: v.str.req,
										frequency	: v.num.str.default( 1000 ),
										qFactor		: v.num.str.default( Math.sqrt( 2 ) ),
										gain		: v.num.str.default( 0 ),
										type		: v.str,
										enabled		: v.bool.default( false ),
										state		: v.str.default( 'disabled' ).enum( 'disabled', 'enabled', 'error' ),
										error		: v.array.like( v.str )
									})
};

const impossibleSchema = { a : v.req.str.default( 25 ) };

const objects = {
		'rawStr' 			: 'a'
	,	'rawArr' 			: ['foo']
	,	'str' 				: { a : 'foo' }
	,	'str2' 				: { a : 'bar' }
	,	'strLen5'			: { a : 'abcde' }
	,	'strLen15'			: { a : 'abcdefghijklmno' }
	,	'num' 				: { a : 0 }
	,	'num1' 				: { a : 1 }
	,	'num5' 				: { a : 5 }
	,	'num100'			: { a : 100 }
	,	'numInf'			: { a : Number.POSITIVE_INFINITY }
	,	'numNaN'			: { a : NaN }
	,	'bool'				: { a : true }
	,	'date'				: { a : new Date() }
	,	'strDate'			: { a : '2011-04-11' }
	,	'badDate'			: { a : '2010411' }
	,	'email'				: { a : 'kevin.kleine@clerksystems.com' }
	,	'badEmail'			: { a : 'kevin@yoyo' }
	,	'object'			: { a : {} }
	,	'noObject'			: { a : '{}' }
	,	'null'				: { a : null }
	,	'emptyString'		: { a : '' }
	,	'array'				: { a : [] }
	,	'array1'			: { a : [ 'a' ] }
	,	'array5'			: { a : new Array(5) }
	,	'array15'			: { a : new Array(15) }
	,	'arrayOfNums'		: { a : [ 3, 6, 1, 7777 ] }
	,	'arrayOfNumAndStr'	: { a : [ 3, 6, 1, 'ddd', 33 ] }
	,	'arrayOfMixed'		: { a : [ 3, new Date(), 1, 'ddd', 33 ] }
	,	'arrayOfStrings'	: { a : [ 'a', 'b', 'c' ] }
	,	'empty'				: {}
	,	'nestedNumber'		: { a : { 'b' : 10 } }
	,	'nestedStr'			: { a : { 'b' : 'yo' } }
	,	'nestedStr2'		: { a : { 'c' : 'yo' } }
	,	'goodLock'			: { l : 'I am locked' }
	,	'badLock'			: { l : 'I am changed vito!' }
	,	'filters'			: {
								"zcztzxrfjp817ezg": {
									gain			: 8.6,
									name			: "",
									frequency		: 20000,
									qFactor			: 0.35,
									type			: "peaking 2nd-order",
									enabled			: true
								},
								"eyivigx21w5vrvei": {
									gain			: 3,
									name			: "",
									frequency		: 21500,
									qFactor			: 0.15,
									type			: "peaking 2nd-order",
									enabled			: true
								}
							}
};

const errorFrom = function()
{
	var a = [ ...arguments ];

	try
	{
		a.shift().call( this, ...a );
	}
	catch ( error )
	{
		return error.message;
	}

	return false;
}



describe( 'ValueValidator', () =>
{
	describe( 'expressions', () =>
	{
		it( 'should turn expressions into validation objects', () =>
		{
			expect( v.req ).to.be.an( 'object' );
		});


		it( 'should allow setting values to required or optional by getting req, opt, etc', () =>
		{
			expect( v.req ).to.have.property( 'isRequired', true );
			expect( v.required ).to.have.property( 'isRequired', true );
			expect( v.opt ).to.have.property( 'isRequired', false );
			expect( v.optional ).to.have.property( 'isRequired', false );
		});


		it ( 'should allow adding allowed types to the validation object, by getting them', () =>
		{
			expect( v.string.allowedTypes.has( 'string' ) ).to.be.true;
			expect( v.email.or.emptyString.allowedTypes.has( 'emptyString' ) ).to.be.true;
		});


		it ( 'should allow adding default values', () =>
		{
			expect( v.string.default( 'piet' ).defaultValue ).to.equal( 'piet' );
		});
	});

	describe ( 'validation', () =>
	{
		const tests = [
			[ 'rawStr', 				'rawStr',				true ]
		,	[ 'rawArr', 				'rawArr',				true ]
		,	[ 'str', 					'str', 					true ]
		,	[ 'num', 					'num', 					true ]
		,	[ 'bool', 					'bool',					true ]
		,	[ 'date', 					'strDate', 				true ]
		,	[ 'str', 					'strDate', 				true ]
		,	[ 'email', 					'email', 				true ]
		,	[ 'object', 				'object', 				true ]
		,	[ 'emptyString', 			'emptyString',			true ]
		,	[ 'null', 					'null',					true ]
		,	[ 'array', 					'array',				true ]
		,	[ 'arrayOfString',			'arrayOfStrings',		true ]
		,	[ 'arrayOrObject',			'array',				true ]
		,	[ 'arrayOrObject',			'object',				true ]
		,	[ 'optStr',					'empty',				true ]
		,	[ 'nestedNumber',			'nestedNumber',			true ]
		,	[ 'optNestedNumber',		'nestedNumber',			true ]
		,	[ 'optNestedNumber',		'empty',				true ]
		,	[ 'arrayedNumber',			'arrayOfNums',			true ]
		,	[ 'arrayedAny',				'arrayOfMixed',			true ]
		,	[ 'array',					'arrayOfNums',			true ]
		,	[ 'array',					'arrayOfMixed',			true ]
		,	[ 'locked',					'goodLock',				true ]

		,	[ 'min4Str',				'email',				true ]
		,	[ 'max10Str',				'str',					true ]
		,	[ 'min4Array',				'array5',				true ]
		,	[ 'max10Array',				'array1',				true ]
		,	[ 'min4Num',				'num5',					true ]
		,	[ 'max10Num',				'num5',					true ]

		,	[ 'objValues',				'nestedStr',			true ]
		,	[ 'objValues',				'nestedStr2',			true ]
		,	[ 'objValues',				'object',				true ]

		,	[ 'objValuesOpt',			'object',				true ]
		,	[ 'objValuesObj',			'object',				true ]

		,	[ 'filter',					'empty',				true ]
		,	[ 'filter',					'object',				true ]
		,	[ 'filter',					'filters',				true ]
		,	[ 'filterReq',				'filters',				true ]
		,	[ 'filterReq',				'empty',				true ]
		,	[ 'reqStr',					'emptyString',			true ]
		,	[ 'filterReq',				'object',				false ]



		,	[ 'rawStr', 				'str',					false ]
		,	[ 'rawArr', 				'str',					false ]
		,	[ 'rawArr', 				'object',				false ]
		,	[ 'object', 				'date',					false ]
		,	[ 'str', 					'num',	 				false ]
		,	[ 'str', 					'bool',	 				false ]
		,	[ 'str', 					'date',	 				false ]
		,	[ 'num', 					'str',	 				false ]
		,	[ 'num', 					'bool',	 				false ]
		,	[ 'email', 					'badEmail',				false ]
		,	[ 'object', 				'noObject',				false ]
		,	[ 'object', 				'null',					false ]
		,	[ 'emptyString', 			'null',					false ]
		,	[ 'array', 					'object',				false ]
		,	[ 'object', 				'array',				false ]
		,	[ 'reqStr',					'empty',				false ]
		,	[ 'nestedNumber',			'nestedStr',			false ]
		,	[ 'optNestedNumber',		'nestedStr',			false ]
		,	[ 'arrayOfString',			'arrayOfNums',			false ]
		,	[ 'arrayedNumber',			'arrayOfNumAndStr',		false ]
		,	[ 'arrayedNumber',			'arrayOfMixed',			false ]
		,	[ 'locked',					'badLock',				false ]
		,	[ 'objValues',				'nestedNumber',			false ]
		,	[ 'max10Str',				'email',				false ]
		,	[ 'min4Array',				'array1',				false ]
		,	[ 'max10Array',				'array15',				false ]
		,	[ 'min4Num',				'num1',					false ]
		,	[ 'max10Num',				'num100',				false ]
		,	[ 'num',					'numInf',				false ]
		,	[ 'num',					'numNaN',				false ]
		];

		tests.forEach( ( test ) =>
		{
			it ( 'should ' + ( test[ 2 ] ? '' : 'NOT ' ) + 'validate test schema ' + test[ 0 ] + ' with test object ' + test[ 1 ], () =>
			{
				const result = errorFrom( validator.validate, schemas[ test[ 0 ] ], objects[ test[ 1 ] ] );

				if ( test[ 2 ] === true )
				{
					expect( result ).to.equal( false, result );
				}
				else
				{
					expect( result ).to.be.a( 'string', test[ 0 ] + ' -> ' + test[ 1 ] + ' ' + result );
				}
			});
		});


		it( 'should only allow settings from enum options, if defined', () =>
		{
			expect( errorFrom( validator.validate, schemas[ 'enum' ], objects[ 'str' ] ) ).to.be.false;
			expect( errorFrom( validator.validate, schemas[ 'enum' ], objects[ 'str2' ] ) ).to.be.a( 'string' );
		});
	});

	describe( 'generator', () =>
	{
		Object.keys( schemas ).forEach( ( schema ) =>
		{
			it ( 'for test schema ' + schema, () =>
			{
				const result = errorFrom( validator.validate, schemas[ schema ], generate( schemas[ schema ] ) );

				expect( result ).to.equal( false, result );
			});
		});


		it( 'should prefer default values given in the expression over default type values', () =>
		{
			expect( generate( schemas[ 'reqDefaultString' ] ) ).to.deep.equal( { 'a' : 'default string 1' } );
			expect( generate( schemas[ 'optDefaultString' ] ) ).to.deep.equal( { 'a' : 'default string 2' } );
			expect( generate( schemas[ 'defaultFalse' ] ) ).to.deep.equal( { 'a' : false } );
			expect( generate( schemas[ 'enumWithDefault' ] ) ).to.deep.equal( { 'a' : 'faa' } );
			expect( generate( schemas[ 'enumWithDefault2' ] ) ).to.deep.equal( { 'a' : false } );
		});

		it ( 'should throw an error for un-generatable schemas', () =>
		{
			expect( errorFrom( validator.generate, impossibleSchema ) ).to.be.a( 'string' );
		});

		it ( 'should be able to generate non-object schemas', () =>
		{
			expect( generate( schemas[ 'rawArr' ] ) ).to.deep.equal( [] );
		});

		it( 'should generate correct type schemas', () =>
		{
			const typeSchema = validator.toObject( schemas[ 'user' ], '', 'types' )
			expect( typeSchema.subValue.clothes.subValue.trousers.options.length ).to.equal( 2 )
			expect( typeSchema.subValue.hobbies.subValue.allowedTypes ).to.deep.equal( [ 'string' ] )
			// console.log( JSON.stringify( typeSchema, null, 2 ) )
		});
	});





	it( 'should take min/max settings into account' );

	it( 'should reset resetOnLoad entries on load', () =>
	{
		const schema 		= {
								'leave' 	: v.string.default( 'LEAVE' )
							,	'reset'		: v.string.resetonload.default( 'RESET' )
							,	'reset2'	: v.num.resetonload
							,	'reset3'	: v.obj.resetonload
							}
		const obj			= {
								'leave' 	: 'different1'
							,	'reset' 	: 'different2'
							,	'reset2'	: 'different3'
							}

		const resetted		= validator.onLoadReset( schema, obj )

		// console.log( 'RESETTED', JSON.stringify( resetted, null, 2 ) )

		expect( resetted.leave ).to.equal( obj.leave )
		expect( resetted.reset ).to.equal( 'RESET' )
		expect( resetted.reset2 ).to.equal( 1 )
		expect( resetted.reset3 ).to.deep.equal( {} )
	})

});
