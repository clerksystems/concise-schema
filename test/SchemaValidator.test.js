'use strict'

const ValueValidator= require( '../src/ValueValidator.js' )
const v				= new ValueValidator()
const { expect }	= require( 'chai' )
const SchemaValidator = require( '../src/SchemaValidator.js' )

const test			= require( './schemas/test.js' )

const schemas = [
	{
		foo: {
			bar: {
				string: v.str,
				number: v.num
			}
		},
		oof: {
			rab: {
				bool: v.bool
			}
		}
	},
	{
		zoom: {
			quux: {
				string: v.str,
				number: v.num
			}
		},
		bool: v.bool.default( false )
	},
	{
		zoom: {
			quux: {
				string: v.str,
				number: v.num
			}
		},
		bool: v.bool,
		zap: {
			zip: {
				bzz: v.str.default('bzzZzzzZrzzZ')
			}
		}
	}
]

const states = [
	{
		foo: {
			bar: {
				string: 'foobar',
				number: 100
			}
		},
		oof: {
			rab: {
				bool: true
			}
		}
	}
]

const migrations = [
	{},
	{
		'zoom.quux': 'foo.bar',
		'bool': 'oof.rab.bool'
	},
	{
		'zoom'			: [
							'zap',
							'zip',
							'foo',
						],
		'zoom.quux'		: [
							'zoom.bar',
							'zip.bar',
							'foo.bar',
						],
		'bool'			: 'oof.rab.bool',
		'zap.zip.bzz'	: [
							'zoom.bar.string',
							'zip.bar.string',
							'foo.bar.string',
						]
	}
]


describe( 'SchemaValidator', function(){

	it( 'loads', function(){
		expect( SchemaValidator ).to.be.an( 'object' )
	})

	it( 'has props', function(){
		for( let prop of [ 'onLoadReset', 'validate', 'generate' ] ){
			expect( SchemaValidator[ prop ] ).not.to.be.undefined
		}
	})

	describe( 'onLoadReset', function(){

		it( 'matching state and schema', function(){
			const result = SchemaValidator.onLoadReset( schemas[0], states[0] )

			expect( result.foo ).not.to.be.undefined
			expect( result.foo.bar ).not.to.be.undefined
			expect( result.foo.bar.string ).to.be.a( 'string' )
			expect( result.foo.bar.string ).to.equal( 'foobar' )
			expect( result.foo.bar.number ).to.be.a( 'number' )
			expect( result.foo.bar.number ).to.equal( 100 )

			expect( result.oof ).not.to.be.undefined
			expect( result.oof.rab ).not.to.be.undefined
			expect( result.oof.rab.bool ).to.be.a( 'boolean' )
			expect( result.oof.rab.bool ).to.equal( true )

		})

		it( 'mismatching state and schema should return defaults from the schema', function(){
			const result = SchemaValidator.onLoadReset( {
				oldState: states[0],
				originalState: states[0],
				schema: schemas[1]
			})

			expect( result.zoom ).not.to.be.undefined
			expect( result.zoom.quux ).not.to.be.undefined
			expect( result.zoom.quux.string ).to.be.a( 'string' )
			expect( result.zoom.quux.string ).to.equal( '' )
			expect( result.zoom.quux.number ).to.be.a( 'number' )
			expect( result.zoom.quux.number ).to.equal( 1 )

			expect( result.bool ).to.be.a( 'boolean' )
			expect( result.bool ).to.equal( false )

		})

		it( 'mismatching state and schema with migrationSchema should return the stored values', function(){
			const result = SchemaValidator.onLoadReset( {
				oldState: states[0],
				originalState: states[0],
				schema: schemas[1],
				migrations: migrations[1]
			})
			expect( result.zoom ).not.to.be.undefined
			expect( result.zoom.quux ).not.to.be.undefined
			expect( result.zoom.quux.string ).to.be.a( 'string' )
			expect( result.zoom.quux.string ).to.equal( 'foobar' )
			expect( result.zoom.quux.number ).to.be.a( 'number' )
			expect( result.zoom.quux.number ).to.equal( 100 )

			expect( result.bool ).to.be.a( 'boolean' )
			expect( result.bool ).to.equal( true )

		})
		it( 'mismatching state and schema with migrationSchema should return the stored values', function(){
			const result = SchemaValidator.onLoadReset( {
				oldState: states[0],
				originalState: states[0],
				schema: schemas[2],
				migrations: migrations[2]
			})
			expect( result.zoom ).not.to.be.undefined
			expect( result.zoom.quux ).not.to.be.undefined
			expect( result.zoom.quux.string ).to.be.a( 'string' )
			expect( result.zoom.quux.string ).to.equal( 'foobar' )
			expect( result.zoom.quux.number ).to.be.a( 'number' )
			expect( result.zoom.quux.number ).to.equal( 100 )

			expect( result.bool ).to.be.a( 'boolean' )
			expect( result.bool ).to.equal( true )

			expect( result.zap ).not.to.be.undefined
			expect( result.zap.zip ).not.to.be.undefined
			expect( result.zap.zip.bzz ).to.be.a( 'string' )
			expect( result.zap.zip.bzz ).to.equal( 'foobar' )

		})

	})

	describe( '', function(){
		let v1 = null
		it( 'migrates from "" to "1.0.0"', function(){
			const result = SchemaValidator.onLoadReset( {
				oldState: test.states[0],
				schema: test.schemas[1],
				migrations: test.migrations[1]
			})

			v1 = result

			expect( result ).to.be.an( 'object' )
			expect( result.version ).to.equal( '1.0.0' )

			expect( result.meta ).to.be.an( 'object' )

			expect( result.meta.host ).to.be.undefined
			expect( result.meta.hostname ).to.equal( 'Hostname' )

			expect( result.meta.publicIp ).to.be.undefined
			expect( result.meta['public-ip'] ).to.equal( '1.2.3.4' )

			expect( result.meta.startTime ).to.be.undefined
			expect( typeof result.meta['start-time'] ).to.be.a( 'string' )
			expect( /\d{4}-\d{2}-\d{2}.\d{2}:\d{2}:\d{2}\.\d{3}./.test(result.meta['start-time']) ).to.true
		})
		it( 'migrates from "" to "2.0.0"', function(){
			const result = SchemaValidator.onLoadReset( {
				oldState: test.states[0],
				schema: test.schemas[2],
				migrations: test.migrations[2]
			})

			expect( result ).to.be.an( 'object' )
			expect( result.version ).to.equal( '2.0.0' )

			expect( result.meta ).to.be.undefined

			expect( result.hostname ).to.equal( 'Hostname' )

			expect( result['public-ip'] ).to.equal( '1.2.3.4' )

			expect( typeof result['start-time'] ).to.be.a( 'string' )
			expect( /\d{4}-\d{2}-\d{2}.\d{2}:\d{2}:\d{2}\.\d{3}./.test(result['start-time']) ).to.true
		})
		it( 'migrates from "1.0.0" to "2.0.0"', function(){
			const result = SchemaValidator.onLoadReset( {
				oldState: v1,
				schema: test.schemas[2],
				migrations: test.migrations[2]
			})

			expect( result ).to.be.an( 'object' )
			expect( result.version ).to.equal( '2.0.0' )

			expect( result.meta ).to.be.undefined

			expect( result.hostname ).to.equal( 'Hostname' )

			expect( result['public-ip'] ).to.equal( '1.2.3.4' )

			expect( typeof result['start-time'] ).to.be.a( 'string' )
			expect( /\d{4}-\d{2}-\d{2}.\d{2}:\d{2}:\d{2}\.\d{3}./.test(result['start-time']) ).to.true
		})
	})

})
