'use strict'

const v = require( '../../src/index.js' ).value

module.exports = {
	schemas: [
		{
			meta: {
				host: v.string,
				publicIp: v.string,
				startTime: v.date
			},
			connections: v.array.resetonload.like(
				{
					id: v.any,
					remote: v.string,
					port: v.number,
					connected: v.boolean.default( false ),
					close: v.function
				}
			),
			world: v.object
		},
		{
			version: v.string.resetonload.lock.default( '1.0.0' ),
			meta: {
				hostname: v.string,
				'public-ip': v.string,
				'start-time': v.date
			},
			connections: v.array.resetonload.like(
				{
					id: v.any,
					remote: v.string,
					port: v.number,
					connected: v.boolean.default( false ),
					close: v.function
				}
			),
			worldview: v.object
		},
		{
			version: v.string.resetonload.lock.default( '2.0.0' ),
			hostname: v.string,
			'public-ip': v.string,
			'start-time': v.date,
			connections: v.array.resetonload.default( [] ).like(
				{
					id: v.any,
					remote: v.string,
					port: v.number,
					connected: v.boolean.default( false ),
					close: v.function
				}
			),
			worldview: v.object
		}
	],

	migrations: [
		{},
		{
			'meta.hostname': [ 'meta.host' ],
			'meta.public-ip': [ 'meta.publicIp' ],
			'meta.start-time': [ 'meta.startTime' ],
			'worldview': [ 'world' ]
		},
		{
			'hostname': [ 'meta.host', 'meta.hostname' ],
			'public-ip': [ 'meta.publicIp', 'meta.public-ip' ],
			'start-time': [ 'meta.start-time', 'meta.startTime' ]
		}
	],

	states: [
		{
			meta: {
				host: 'Hostname',
				publicIp: '1.2.3.4',
				startTime: new Date()
			},
			connections: [
				{
					id: 'conn-00001',
					remote: 'localhost',
					port: '9876',
					connected: true,
					close: () => {}
				},
				{
					id: 'conn-00002',
					remote: 'localhost',
					port: '9874',
					connected: false,
					close: () => {}
				}
			],
			world: {
				'Klaas': {
					name: 'Klaas'
				},
				'Henk': {
					name: 'Henk'
				}
			}
		}
	]

}
